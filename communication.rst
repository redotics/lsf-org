Communication
#############


Libre Space Foundation uses various channels, technologies and methods to communicate among its members, projects and to the world.

External Communications
***********************

For a list of external communication channels and specifics around the organization of the Communciations Team check out the `Communications Organizational Repo <https://gitlab.com/librespacefoundation/lsf-comms-org>`_ .

Project Communications
**********************

Different project employ different types of communications:

Email
=====

+---------------+---------------------------+------------------------------------------------------------+
| Project       | Email                     | Description                                                |
+===============+===========================+============================================================+
| LSF - Generic | info@libre.space          | Central email for all LSF related inquiries                |
+---------------+---------------------------+------------------------------------------------------------+
| LSF - Generic | shop@libre.space          | Online shop related emails                                 |
+---------------+---------------------------+------------------------------------------------------------+
| LSF - Generic | ops@libre.space           | Infrastructure related emails                              |
+---------------+---------------------------+------------------------------------------------------------+
| LSF - Generic | ssa@libre.space           | Space Situational Awareness team email                     |
+---------------+---------------------------+------------------------------------------------------------+
| LSF - Generic | moc@libre.space           | Mission Operations Center email                            |
+---------------+---------------------------+------------------------------------------------------------+
| LSF - Generic | all@libre.space           | All LSF core members alias (restricted to LSF emails only) |
+---------------+---------------------------+------------------------------------------------------------+
| LSF - Generic | bills@librespace.odoo.com | Incoming email for Odoo accounting system                  |
+---------------+---------------------------+------------------------------------------------------------+
|               |                           |                                                            |
+---------------+---------------------------+------------------------------------------------------------+

Real-time communications
========================

+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------+--------------+
| Project        | Channel                                                                                          | Description                                                                | Access       |
+================+==================================================================================================+============================================================================+==============+
| LSF - Generic  | `#librespace:matrix.org <https://app.element.io/?#/room/#librespace:matrix.org>`_                | Generic channel for all LSF projects                                       | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------+--------------+
| LSF - Generic  | `#lsf-ops:matrix.org <https://app.element.io/?#/room/#lsf-ops:matrix.org>`_                      | Infrstructure team channel                                                 | Invite-only  |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------+--------------+
| LSF - Generic  | `#lsf-core:matrix.org <https://app.element.io/?#/room/#lsf-core:matrix.org>`_                    | Channel for all LSF Core members                                           | Invite-only  |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------+--------------+
| LSF - Generic  | `#librespace-comms:matrix.org <https://app.element.io/?#/room/#librespace-comms:matrix.org>`_    | Communciation team channel                                                 | Invite-only  |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------+--------------+
| LSF - Generic  | `#librespace-board:matrix.org <https://app.element.io/?#/room/#librespace-board:matrix.org>`_    | LSF Board channel                                                          | Invite-only  |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------+--------------+
| SatNOGS        | `#satnogs:matrix.org <https://app.element.io/?#/room/#satnogs:matrix.org>`_                      | SatNOGS project main channel                                               | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------+--------------+
| SatNOGS        | `#satnogs-dev:matrix.org <https://app.element.io/?#/room/#satnogs-dev:matrix.org>`_              | SatNOGS Developers channel                                                 | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------+--------------+
| SatNOGS        | `#satnogs-missions:matrix.org <https://app.element.io/?#/room/#satnogs-missions:matrix.org>`_    | Channel for missions using SatNOGS                                         | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------+--------------+
| SatNOGS        | `#satnogs-operators:matrix.org <https://app.element.io/?#/room/#satnogs-operators:matrix.org>`_  | Channel for SatNOGS scheduling and operators                               | Invite-only  |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------+--------------+
| SatNOGS        | `#satnogs-comms:matrix.org <https://app.element.io/?#/room/#satnogs-comms:matrix.org>`_          | Channel for the SatNOGS COMMS cubesat subsystem project                    | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------+--------------+
| SIDLOC         | `#sidloc:matrix.org <https://app.element.io/?#/room/#sidloc:matrix.org>`_                        | Channel for the SIDLOC spacecraft identification and localization project  | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------+--------------+
| PQ9ISH         | `#PQ9ISH:matrix.org <https://app.element.io/?#/room/#PQ9ISH:matrix.org>`_                        | Channel for the PQ9ISH Pocketqube standard and compatible projects         | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------+--------------+
| QUBIK          | `#qubik:matrix.org <https://app.element.io/?#/room/#qubik:matrix.org>`_                          | Channel for QUBIK and PICOBUS, pocketqube LSF projects                     | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------+--------------+
| stvid          | `#stvid:matrix.org <https://app.element.io/?#/room/#stvid:matrix.org>`_                          | Channel for the stvid, optical ssa observations project                    | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------+--------------+
| OCSW           | `#oscw-public:matrix.org <https://app.element.io/?#/room/#oscw-public:matrix.org>`_              | Channel for the Open Source Cubesat Workshop                               | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------+--------------+
| OSCW           | `#oscw:matrix.org <https://app.element.io/?#/room/#oscw:matrix.org>`_                            | Channel for the OSCW organization committee                                | Invite-only  |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------+--------------+
| Polaris        | `#polaris:matrix.org <https://app.element.io/?#/room/#polaris:matrix.org>`_                      | Channel for the Polaris machine learning project                           | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------+--------------+
| Metasat        | `#metasat:matrix.org <https://app.element.io/?#/room/#metasat:matrix.org>`_                      | Channel for the Metasat project                                            | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------+--------------+
| Cronos Rocket  | `#cronos-rocketry:matrix.org <https://app.element.io/?#/room/#cronos-rocketry:matrix.org>`_      | Channel for the Cronos Rocketry project                                    | Open         |
+----------------+--------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------+--------------+
