=================
Core Contributors
=================

Volunteer and paid contributors to LSF that have showcased substantial contributions in one or more projects of the Foundation are invited by the board to participate in the "Core Contributors" group.

Current "Core Contributors" can be found in the `About Us <https://libre.space/about-us/>`_ section of the Libre Space website.

Being a member of "Core Contributors" grants the individual the following privileges and duties:

#. Recognition in libre.space website About section
#. Access to private "Core Contributors" discussion group in community.libre.space
#. Access to #lsf-core matrix channel
#. An email account of @libre.space format
#. Membership to LSF Core group on GitLab
#. Access to cloud.libre.space
#. Participation to LSF All-Hands (annual get together of all core contributors)
#. Commitment to represent, spread and uphold the Principles and Pillars of the Libre Space Manifesto

Joining
-------

Joining the "Core Contributors" group requires a nomination by an existing member and approval by the LSF board.

Onboarding
----------

Once a new member is approved for the "Core Contributors" group the following actions should happen:

- creation of the email account and instructions for setup sent
- addition of the email account to the appropriate aliases
- invite the core contributor to add the email account in hr email alias
- grant access to appropriate Gitlab groups and repositories
- invite to appropriate matrix.org channels
- addition to lsf-core group on community.libre.space
- creation of cloud.libre.space account and appropriate group membership
- access to specific tools depending on role (social accounts, devops, monitoring etc)
- updating of About Us page on libre.space
- updating of lsf-contributors.ods with details of the process
- email to all LSF welcoming the new member
