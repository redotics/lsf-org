Projects Portfolio
##################

.. contents::
   :local:
   :backlinks: none

.. _Pierros Papadeas: https://gitlab.com/pierros
.. _Vasilis Tsiligiannis: https://gitlab.com/acinonyx
.. _Manolis Surligas: https://gitlab.com/surligas
.. _Eleftherios Kosmas: https://gitlab.com/elkosmas
.. _Manthos Papamatthaiou: https://gitlab.com/papamat
.. _Xabi Crespo: https://gitlab.com/crespum
.. _George Tsagkarelis: https://gitlab.com/georgetsag
.. _Michail Raptakis: https://gitlab.com/mraptakis
.. _Nikoletta Triantafyllopoulou: https://gitlab.com/letria
.. _Ilias Daradimos: https://gitlab.com/drid

Overview
********

Libre Space Foundation develops and supports a variety of open-source projects for space.
For each project there is a project manager and a project champion (more on those :doc:`roles`).


Currently active projects
*************************

The following major projects are in active development:

+------------------+--------------------------+---------------------------------+
| Project          | Project Manager          | Project Champion                |
+==================+==========================+=================================+
| `SatNOGS`_       | `Vasilis Tsiligiannis`_  | `Vasilis Tsiligiannis`_         |
+------------------+--------------------------+---------------------------------+
| `tiny-ipfs`_     | `Manolis Surligas`_      | `George Tsagkarelis`_           |
+------------------+--------------------------+---------------------------------+
| `SatNOGS-tiny`_  | `Manolis Surligas`_      |                                 |
+------------------+--------------------------+---------------------------------+
| `SIDLOC`_        | `Pierros Papadeas`_      | `Vasilis Tsiligiannis`_         |
+------------------+--------------------------+---------------------------------+
| `QUBIK 3-6`_     | `Pierros Papadeas`_      | `Manthos Papamatthaiou`_        |
+------------------+--------------------------+---------------------------------+
| `PICOBUS`_       | `Manthos Papamatthaiou`_ | `Manthos Papamatthaiou`_        |
+------------------+--------------------------+---------------------------------+
| `SatNOGS COMMS`_ | `Pierros Papadeas`_      | `Pierros Papadeas`_             |
+------------------+--------------------------+---------------------------------+
| Communications   | `Eleftherios Kosmas`_    | `Nikoletta Triantafyllopoulou`_ |
+------------------+--------------------------+---------------------------------+
| Rocketry         | `Manthos Papamatthaiou`_ | `Ilias Daradimos`_              |
+------------------+--------------------------+---------------------------------+
| Infrastructure   | `Vasilis Tsiligiannis`_  | `Vasilis Tsiligiannis`_         |
+------------------+--------------------------+---------------------------------+
| `Polaris`_       | `Xabi Crespo`_           | `Eleftherios Kosmas`_           |
+------------------+--------------------------+---------------------------------+

.. _SatNOGS: https://gitlab.com/groups/librespacefoundation/satnogs/
.. _OpenSatCom: https://opensatcom.org
.. _tiny-ipfs: 
.. _SatNOGS-tiny:
.. _SIDLOC:
.. _QUBIK 3-6:
.. _PICOBUS:
.. _SatNOGS COMMS:

.. _Polaris: https://gitlab.com/librespacefoundation/polaris

Other projects can be found in lsf-core repository.


Previously active projects
**************************

The following major projects have previously been in active development:

+---------------+---------------------+------------------+
| Project       | Project Manager     | Project Champion |
+===============+=====================+==================+
| `OpenSatCom`_ | `Pierros Papadeas`_ | N/A              |
+---------------+---------------------+------------------+
| UPSat_        | `Pierros Papadeas`_ | N/A              |
+---------------+---------------------+------------------+


.. _UPSat: https://gitlab.com/groups/librespacefoundation/satnogs/_client-and-network/-/shared


Contributors
************

Libre Space Foundation is an active and welcoming community around open source space projects.
We welcome all contributors in our projects and repositories.
We recognize those contributors that have been making considerable contributions in our projects by inviting them in our "LSF Core Contributors" group.
Learn more about :doc:`../people/core-contributors`.

