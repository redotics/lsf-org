Roles
#####

.. contents::
   :local:
   :backlinks: none

Project Manager
***************

While there are different scales of projects in LSF, in the broadest sense, project managers (PMs) are responsible for planning, organizing, and directing the completion of specific projects for LSF while ensuring these projects are on time, on budget, and within scope.

Specifics on the tooling and processes can be found in :doc:`project-management`.

Project Champion
****************

In order to ensure efficient and in-time resource allocation, each project employs a project champion role that is responsible to:

* Document project progress and report back to the rest of the org
* Communicate clear priorities and resource needs to the rest of the org
* Timely identify blockers and seek resolutions
* Critically analyzing and ensuring best practices for project management in collaboration with the Project Manager

It is possible that Project Champion and Project Manager is the same person, although it is envisioned as a separate individual for the larger projects of the org.
