Procurement
###########


For various project and activity needs, contributors of LSF need to procure various items or services. Here is a brief outline and some guiding principles

Procurement Process
*******************

1. Identify the need and thing about possible future evolution of this need.
2. Check with Lab (contacting Aris) on possible existing stock
3. Identify the best vendor possible taking into account lower price but also a prioritization with regards to vendor location. The priority is as follows:

  #. EU vendor with valid VAT VIES number
  #. Greek vendor with VAT number
  #. Non-EU vendor with local VAT number (avoid if possible!)
  #. Any vendor without a VAT number (avoid if possible!)

4. Request a proforma invoice to be issued (unless specific vendor below) to LSF Company details.
5. Forward to bank@libre.space for payment via bank transfer or via a card purchase link. We prefer bank transfer over paypal.
6. Once paid, you will receive confirmation from the people behind bank@
7. Once the items arrive, scan and forward the invoice to bills @T librespace.odoo.com

Specific Vendors
****************
The following vendors have been used extensively in the past and should be preferred since we have a special setup (See notes)

+---------------+----------------+----------------------+---------------+------------------------------------+
| Vendor        | Type           | Username             | Access        | Notes                              |
+===============+================+======================+===============+====================================+
| Mouser        | Components     | librespacefoundation | Pierros, Agis | Company account, Details completed |
+---------------+----------------+----------------------+---------------+------------------------------------+
| Digikey       | Components     | pierros@libre.space  | Pierros       | Company account, Details completed |
+---------------+----------------+----------------------+---------------+------------------------------------+
| Farnell       | Components     | librespace           | Pierros       | Company account, Details completed |
+---------------+----------------+----------------------+---------------+------------------------------------+
| Mini Circuits | Components     |                      | Pierros       | Details for export control added   |
+---------------+----------------+----------------------+---------------+------------------------------------+
| JLC PCB       | PCB Production | pierros@libre.space  | Pierros, Agis | Company account, Details completed |
+---------------+----------------+----------------------+---------------+------------------------------------+

If you require a purchase from the vendors above, please reach out dircetly to someone with access.


Company Details
***************

| Libre Space Foundation
| Ampatiellou 11
| Athens
| 11144
| Greece
| +302130210437
| VAT ID: EL997539194
