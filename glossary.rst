Glossary
========

The purpose of this document is to provide a reference for acronyms and terms used in the Libre Space Foundation community and contributors may cross reference any of the supplyied terms.

.. glossary::

 AB
  Abstract

 ABCL
  As-built Configuration Data List
 
 AC
  Alternating Current
 
 ACI
  Adjacent Channel Interference

 ACS
  Attitude Control System

 ACU
  Antenna Control Unit

 ADC
  Analog to Digital Converter

 AFSK
  Audio Frequency Shift Keying

 AFT
  Abbreviated Functional Test

 AGC
  Automatic Gain Control

 AGPL
  Affero General Public Licence

 AI
  Artificial Intelligence

 AIT
  Assembly Integration and Testing

 AIV
  Assembly Integration and Verification

 AM
  Amplitude Modulation

 AMSAT
  Amateur Satellite

 AOCS
  Attitude and Orbit Control System

 AOS
  May refer to:
  Acquisition Of Signal or, 
  Advanced Orbiting Systems

 API
  Application Programming Interface

 APT
  Automatic Picture Transmission

 ARRL
  American Radio Relay League

 ARTES
  Advanced Research in Telecommunications Systems

 ASIC
  Application-Specific Integrated Circuit

 ASW
  Address and Synchronization Word

 AXI
  Advanced eXtensible Interface

 B2B
  Business to Business

 B2C
  Business to Consumer

 BaLun
  Balanced to Unbalanced

 BMU
  Battery Management Unit

 BER
  Bit Error Rate

 BOM
  Bill Of Materials

 BR
  Brochure

 BPF
  Band Pass Filter

 BPbis
  Bundle Protocol

 BPSK
  Binary Phase Shift Keying

 BSD
  Berkeley Software Distribution

 CAD
  Computer Aided Design

 CAN
  Controller Area Network

 CANFD
  Controller Area Network flexible data-rate

 CC-BY-SA
  Creative Commons Attribution-ShareAlike

 CCD
  Contract Closure Documentation

 CCI
  Co-channel Interference

 CCSDS
  Consultative Committee for Space Data Systems

 CDR
  Critical Design Review

 CEPT
  European Conference of Postal and Telecommunications Administrations

 CERN
  Center for European Nuclear Research

 CERN-OHL
  :term:`CERN` Open Hardware Licence

 CFDP
  :term:`CCSDS` File Delivery Protocol

 CIDL
  Configuration Item Data List

 CNC
  Computer numerical control

 COLDSUN
  COmmunications reLay for Deep-Sea Underwater Networks

 COMMS
  Communications

 CONOPS
  Concept of Operations

 COTS
  Commercial off the Self

 CPLD
  Complex Programmable Logic Device

 CPU
  Central Processing Unit

 CRC
  Cyclic Redundancy Check

 CSS
  Cascading Style Sheet

 CW
  Continuous Wave

 D
  Deliverable

 DAC
  Digital to Analog Converter

 DB
  Database

 DC
  Direct Current

 DL
  Deep Learning

 DM
  Development Model

 DP
  Data Package

 DSN
  Deep Space Network

 DSP
  Digital Signal Processing

 DTN
  Delay and Disruption-Tolerant Networking

 DUT
  Device Under Test

 DUV
  Data Under Voice

 DVB-RCS
  Digital Video Broadcasting - Return Channel via Satellite

 DVB-S2
  Digital Video Broadcasting - Satellite -Second Generation

 E2E
  End-to-End

 ECC
  Error-correcting code

 ECSS
  European Cooperation for Space Standardization

 EDAC
  Error Detection And Correction

 EDEN
  :term:`EGSE` Data Exchange Network

 EDRS
  European Data Relay Satellite System

 EDV
  Energy Detection Value

 EGSE
  Electrical Ground Support Equipment

 EIRP
  Equivalent isotropically radiated power

 EM
  May refer to:
  a) Engineering Model 
  b) Electrical Model

 EMC
  ElectroMagnetic Compatibility

 EMI
  ElectroMagnetic Interference

 eMMC
  embedded Multi-Media Controller

 EMS
  Emergency Management Services

 ENBW
  Equivalent noise bandwidth

 ENOB
  Effective number of bits

 EO
  Earth Observing

 EPS
  Electrical Power System

 ESA
  European Space Agency

 ESOC
  European Space Operations Center

 ESR
  Executive Summary Report

 ESSB
  :term:`ESA` Standardization Steering Board

 ESTEC
  European Space Research and Technology Center

 FEA
  Finite Element Analysis

 FEC
  Forward Error Correction

 FFT
  Fast Fourier Transform

 FM
  May refer to:
  Flight Model or,
  Frequency Modulation

 FMC
  FPGA Mazzanine Card

 FOC
  Full Operational Capability

 FOSDEM
  Free and Open Source Developers European Meeting

 FOR
  Frame Of Reference

 FOV
  Field of View

 FP
  Final Presentation

 FPGA
  Field Programmable Gate Array

 FR
  Final Report

 FSK
  Frequency Shift Keying

 GCC
  GNU Compiler Collection

 GEO
  Geostationary Equatorial Orbit

 GESTRA
  German Experimental Space Surveillance and Tracking Radar

 GFSK
  Gaussian Frequency Shift Keying

 GLONASS
  Global Navigation Satellite System

 GMAT
  General Mission Analysis Tool

 GNSS
  Global Navigation Satellite System

 GPCPU
  General Purpose :term:`CPU`

 GPL
  GNU General Public License

 GPS
  Global Positioning System

 GPSDO
  :term:`GPS`-Disciplined Oscillator

 GRAVES
  Grand Réseau Adapté à la Veille Spatiale

 gRPC
  gRPC Remote Procedure Calls

 GS
  Ground Station

 GSE
  Ground Support Equipment

 GSPS
  Giga Samples per Second

 GSoC
  Google Summer of Code

 GUI
  Graphical User Interface

 HAL
  Hardware Abstraction Layer

 HDF5
  Hierarchical Data Format

 HDL
  Hardware Description Language

 HEEQ
  Heliocentric Earth Equatorial

 HEO
  Highly Elliplical Orbit

 HFT
  Hidden Factors as Topics

 HPA
  High Power Amplifier

 HPC
  May refer to:
  a) High-performance Computing
  b) High Pin Count

 HTML
  Hyper Text Markup Language

 HW
  Hardware

 I2C
  Inter-Integrated Circuit

 IARU
  International Amateur Radio Union

 IC
  Integrated Circuit

 ICT
  Information and Communication Technology

 IEEE
  Institute of Electrical and Electronics Engineers

 IESL
  Institue of Electronic Structure and Laser

 IF
  Intermediate Frequency

 IFFT
  Inverse Fast Fourier Tranform

 IO
  Input/Output

 IOD
  In-Orbit Demostration

 IOT
  Internet Of Things

 IQ
  In-phase/Quadrature

 IP
  Implementation Plan

 IP3
  Third-order intercept point

 IPND
  IP-based Neighbor Discovery

 IPv4
  Internet Protocol version 4

 IPv6
  Internet Protocol version 6

 ISI
  Intersymbol Interference

 ISM
  Industrial, Scientific, and Medical

 ISON
  International Scientific Optical Network

 ISS
  International Space Station

 ITT
  Invitation To Tender

 ITU
  International Telecommunication Union

 ITU-R
  International Telecommunication Union - Radiocommunications

 ITU RR
  ITU Radio Regulations

 JAXA
  Japan Aerospace Exporation Agency

 JPL
  Jet Propulsion Laboratory

 KO
  Kick-Off

 KOM
  Kick-Off Meeting

 LCNS
  Lunar Communication and Navigation System

 LGPL
  GNU Lesser General Public License

 LEDSAT
  LED-based Small SATellite

 LEO
  Low Earth Orbit

 LEOP
  Launch and Early Orbit Phase

 LFSR
  Linear-Feedback Shift Register

 LNA
  Low Noise Amplifier

 LO
  Local Oscillator

 LOS
  May refer to
  a) Line of Sight 
  b) Loss Of Signal

 LRPT
  Low-Resolution Picture Transmission

 LRR
  Laser Retro Reflector

 LSF
  Libre Space Foundation

 LSTM
  Long Short-Term Memory

 LSTN
  Library Space Technology Network

 LVDS
  Low Voltage Differential Signalling

 LVTTL
  Low Voltage TTL

 M2M
  Machine To Machine

 MCMC
  Markov Chain Monte Carlo

 MCU
  Micro-Controller Unit

 MDS
  Minimum Detectable Signal

 MEO
  Medium Earth Orbit

 MEX
  Mars Express

 MGSE
  Mechanical Ground Support Equipment

 MIB
  Mission Information Base

 MIMO
  Multiple-Input, Multiple-Output

 MIT
  Massachusetts Institute of Technology

 ML
  Machine Learning

 MLRR
  Modulated Laser Retro-Reflector

 MoM
  Minutes of Meeting

 MOOC
  Massive Open Online Course

 MPL
  Mozilla Public License

 MPR
  Monthly Progress Reports

 MRG
  Microelectronics Research Group

 MRR
  Modulated Laser Retro-Reflector

 MS
  Milestone

 MSK
  Minimum Shift Keying

 MSPS
  Mega Samples per Symbol

 MSS
  Mobile Satellite Service

 NEC2
  Numerical Electromagnetics Code Version 2

 NF
  Noise Figure

 NIST
  National Institute of Standards and Technology

 NORAD
  North American Air Defense Command

 NS-3
  Network Simulator 3

 NSL
  NearSpace Launch

 NTE
  Nanosatellite Tracking Experiment

 OBC
  On-board Computer

 OCS
  Orbit Control System

 ODM
  Orbit Data Message

 OEM
  May refer to:
  a) Original Equipment Manufacturer
  b) Orbit Ephemeris Message

 OOL
  Out-of-Limits

 OOT
  Out-of-Tree (GNURadio Out-of-tree module)

 OPS
  (Space) Operations

 OS
  Operating System

 OS3
  Open Source Satellite Simulator

 OSCW
  Open Source Cubesat Workshop

 OSDLP
  Open Space Data Link Protocol

 OSI
  May refer to
  a) Open Systems Interconnection
  b) Open Source Initiative

 OTP
  One time programmable

 PA
  Power Amplifier

 PAPR
  Peak to Average Power Ration

 PC
  May refer to:
  a) Project Coordinator
  b) Personal Computer

 PCB
  Printed Circuit Board

 PCDU
  Power Conditioning and Distribution Unit

 PCIe
  Peripheral Component Interconnect Express

 PDM
  Product Data Management

 PDR
  Preliminary Design Review

 PLL
  Phased Locked Loop

 PLM
  Product Lifecycle Management

 PMIC
  Power management integrated circuits

 PoC
  Proof of Concept

 PoE
  Power over Ethernet

 POSIX
  Portable Operating System Interface for Unix

 PQ
  PocketQube

 PSK
  Phase-Shift Keying

 PSLV
  Polar Satellite Launch Vehicle

 PV
  Photovoltaics

 QA
  Quality Assurance

 QPSK
  Quadrature Phase Shift Keying

 QSFP
  Quad Small Form-factor  Pluggable transceiver

 R&D
  Research & Development

 RD
  Reference Document

 RHCP
  Right Hand Circular Polarized

 RF
  Radio Frequency

 RFMC
  RF Mezzanine Card

 RFSoC
  RF System on Chip

 RILDOS
  Radio with Identity and Location Data for Operations and SSA

 RPC
  Remote Procedure Calls

 RR
  Requirements Review

 RSSI
  Received Signal Strength Indication

 RTC
  Real Time Clock

 RW
  Reaction Wheel

 RX
  Receive

 SATA
  Serial Advanced Technology Attachment (hard disk interface)

 SATCOM
  Satellite Communication

 SatNOGS
  Satellite Network Open Ground-Station

 SAW
  Surface Acoustic Wave

 SBC
  Single Board Computer

 SC
  Spacecraft

 SDA
  Space Data Association

 SDLS
  Space Data Link Security

 SDN
  Software Defined Networking

 SDR
  May refer to:
  a) Software Defined Radio
  b) System Desing Review

 SGP4
  Standard General Perturbations Satellite Orbit Model 4

 SEE
  Single-event effects

 SESP
  Simulation for European Space Programmes

 SEU
  Single-event upset

 SFPT
  Satellite Functions and Performance Test

 SHF
  Super High Frequency (3-30 GHz)

 SIDLOC
  Satellite Identification and Localisation

 SiDs
  Simple Downlink Share Convention Protocol

 SigMF
  Signal Metadata Format

 SIMD
  Single Instruction, Multiple Data

 SMA
  SubMiniature version A

 SMT
  Surface Mount Technology

 SNR
  Signal-to-noise ratio

 SNS
  Satellite Network Simulator

 SNS3
  Satellite Network Simulator

 SoC
  System On a Chip

 SOS
  Space Operation Service

 SoW
  Statement of Work

 SPENVIS
  Space Environment, Effect and Education System

 SPI
  Serial Peripheral Interface

 SPOC
  Spacecraft Operation Center

 SPP
  Space Packet Protocol

 SQNR
  Signal-to-quantization-noise ratio

 SRF
  Software Reuse File

 SRS
  Space Research Service

 SSA
  Space Situational Awareness

 SSN
  Space Surveillance Network

 SSO
  Sun-Synchronous Orbit

 SST
  Space Surveillance and Tracking

 STM
  Space Traffic Management

 SW
  Software

 SWR
  Standing Wave Radio

 TAS
  Technical Achievement Summary

 TBD
  To Be Described

 TC
  Tele-command

 TC&C
  Tele-command & Control

 TCP
  Transmission Control Protocol

 TCXO
  Temperature-compensated crystal oscillator

 TDMA
  Time Division Multiple Access

 TDP
  Technical Data Package

 TID
  Total Ionizing Dose

 TJD
  Truncated Julian Day

 TLE
  Two Line Element set

 TM
  Telemetry

 TMR
  Triple Mode Redundancy

 TMTC
  Telemetry & Tele-control

 TN
  Technical Note

 TRL
  Technology Readiness Level

 TT&C
  Telemetry, Tracking and Command

 TTL
  Transistor-transistor logic

 TVAC
  Thermal Vacuum Chamber

 TWT
  Travelling-wave Tube

 TX
  Transmit

 UART
  Universal Asynchronous Receiver - Transmitter

 UAS
  Unmanned Aircraft Systems

 UHF
  Ultra High Frequency (0.3 - 3 GHz)

 UI
  User Interface

 UNOOSA
  United Nations Office for Outer Space Affairs

 USB
  Universal Serial Bus

 USLP
  Unified Space Link Protocol

 UTC
  Universal Time Co-ordinated

 UX
  User Experience

 V&V
  Verification and Validation

 VCO
  Voltage - Controlled Oscillator

 VGA
  Variable Gain Amplifier

 VHF
  Very High Frequency (30 - 300 MHz)

 VNA
  Vector Network Analyzer

 VOLK
  Vectorized Library of Kernels

 VHDL
  VHSIC Hardware Description  Language

 VPL
  Visual Programming Language

 WBS
  Work Breakdown Structure

 WP
  Work Package

 WPD
  Work Package Description

 WRC
  World Radiocommunication Conferences

 YAML
  YAML Ain’t Markup Language