Libre Space Foundation documentation
====================================

Intro
-----
Welcome to Libre Space Foundation documentation.
In these pages you can find operational principles, definitions, processes and best practices for various internal workings of the Foundation.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   projects/index
   people/index
   event-management
   meetings
   communication
   procurement
   glossary

* :ref:`search`
